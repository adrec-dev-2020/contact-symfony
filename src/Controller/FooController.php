<?php

namespace App\Controller;

use App\Enums\Gender;
use App\Enums\Job;
use App\Enums\PhoneType;
use App\Pattern\Facade\FoobarInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class FooController extends AbstractController
{
    private Environment $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/foo", name="foo")
     */
    public function index(FoobarInterface $foobar): Response
    {
//        Gender::throwOnInvalid(42);

        dump($foobar->getFoo());



        return $this->render('foo/index.html.twig', [
            'controller_name' => 'FooController',
        ]);
    }

    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        if (null === $response) {
            $response = new Response();
        }

        $response->setContent($this->twig->render($view, $parameters));

        return $response;
    }
}
