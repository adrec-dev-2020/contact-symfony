<?php

namespace App\Entity;

use App\Repository\JobInfoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=JobInfoRepository::class)
 */
class JobInfo
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Contact::class, inversedBy="jobs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $contact;

    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     */
    private $job;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startedAt;

    public function getStartedAt(): ?\DateTimeInterface
    {
        return $this->startedAt;
    }

    public function setStartedAt(?\DateTimeInterface $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    public function setContact(?Contact $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getJob(): ?int
    {
        return $this->job;
    }

    public function setJob(int $job): self
    {
        $this->job = $job;

        return $this;
    }
}
