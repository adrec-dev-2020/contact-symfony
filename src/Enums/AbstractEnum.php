<?php

/*
 * This file is part of the contact package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Enums;

/**
 * Class AbstractEnum
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractEnum
{
    public static function checkValue($value): bool
    {
        $ref = new \ReflectionClass(get_called_class());
        return in_array($value, $ref->getConstants());
    }

    public static function throwOnInvalid($value): void
    {
        if (!self::checkValue($value)) {
            $ref = new \ReflectionClass(get_called_class());

            throw new \InvalidArgumentException(sprintf(
                'Wrong value given for enum "%s", allowed: %s',
                get_called_class(),
                implode(', ', array_keys($ref->getConstants()))
            ));
        }
    }
}
