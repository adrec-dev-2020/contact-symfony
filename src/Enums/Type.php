<?php

/*
 * This file is part of the contact package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Enums;

/**
 * Class Type
 *
 * @author Benjamin Georgeault
 */
abstract class Type extends AbstractEnum
{
    const WORK = 0;
    const HOME = 1;
}
