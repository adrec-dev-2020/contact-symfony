<?php

/*
 * This file is part of the contact package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Enums;

/**
 * Class PhoneType
 *
 * @author Benjamin Georgeault
 */
class PhoneType extends Type
{
    const MOBILE = 100;
    const FAX = 101;
}
