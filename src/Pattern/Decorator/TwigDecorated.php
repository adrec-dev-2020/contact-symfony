<?php

/*
 * This file is part of the contact package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Pattern\Decorator;

use Twig\Compiler;
use Twig\Environment;
use Twig\Extension\ExtensionInterface;
use Twig\Lexer;
use Twig\Loader\LoaderInterface;
use Twig\Node\ModuleNode;
use Twig\Node\Node;
use Twig\NodeVisitor\NodeVisitorInterface;
use Twig\Parser;
use Twig\RuntimeLoader\RuntimeLoaderInterface;
use Twig\Source;
use Twig\Template;
use Twig\TemplateWrapper;
use Twig\TokenParser\TokenParserInterface;
use Twig\TokenStream;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\TwigTest;

/**
 * Class TwigDecorated
 *
 * @author Benjamin Georgeault
 */
class TwigDecorated extends Environment
{
    private Environment $decorated;

    public function __construct(Environment $decorated)
    {
        $this->decorated = $decorated;
    }

    public function enableDebug()
    {
        $this->decorated->enableDebug();
    }

    public function disableDebug()
    {
        $this->decorated->disableDebug();
    }

    public function isDebug()
    {
        return $this->decorated->isDebug();
    }

    public function enableAutoReload()
    {
        $this->decorated->enableAutoReload();
    }

    public function disableAutoReload()
    {
        $this->decorated->disableAutoReload();
    }

    public function isAutoReload()
    {
        return $this->decorated->isAutoReload();
    }

    public function enableStrictVariables()
    {
        $this->decorated->enableStrictVariables();
    }

    public function disableStrictVariables()
    {
        $this->decorated->disableStrictVariables();
    }

    public function isStrictVariables()
    {
        return $this->decorated->isStrictVariables();
    }

    public function getCache($original = true)
    {
        return $this->decorated->getCache($original);
    }

    public function setCache($cache)
    {
        $this->decorated->setCache($cache);
    }

    public function getTemplateClass(string $name, int $index = null): string
    {
        return $this->decorated->getTemplateClass($name, $index);
    }

    public function render($name, array $context = []): string
    {
        $context['hello'] = 'coucou';

        return $this->decorated->render($name, $context);
    }

    public function display($name, array $context = []): void
    {
        $this->decorated->display($name, $context);
    }

    public function load($name): TemplateWrapper
    {
        return $this->decorated->load($name);
    }

    public function loadTemplate(string $cls, string $name, int $index = null): Template
    {
        return $this->decorated->loadTemplate($cls, $name, $index);
    }

    public function createTemplate(string $template, string $name = null): TemplateWrapper
    {
        return $this->decorated->createTemplate($template, $name);
    }

    public function isTemplateFresh(string $name, int $time): bool
    {
        return $this->decorated->isTemplateFresh($name, $time);
    }

    public function resolveTemplate($names): TemplateWrapper
    {
        return $this->decorated->resolveTemplate($names);
    }

    public function setLexer(Lexer $lexer)
    {
        $this->decorated->setLexer($lexer);
    }

    public function tokenize(Source $source): TokenStream
    {
        return $this->decorated->tokenize($source);
    }

    public function setParser(Parser $parser)
    {
        $this->decorated->setParser($parser);
    }

    public function parse(TokenStream $stream): ModuleNode
    {
        return $this->decorated->parse($stream);
    }

    public function setCompiler(Compiler $compiler)
    {
        $this->decorated->setCompiler($compiler);
    }

    public function compile(Node $node): string
    {
        return $this->decorated->compile($node);
    }

    public function compileSource(Source $source): string
    {
        return $this->decorated->compileSource($source);
    }

    public function setLoader(LoaderInterface $loader)
    {
        $this->decorated->setLoader($loader);
    }

    public function getLoader(): LoaderInterface
    {
        return $this->decorated->getLoader();
    }

    public function setCharset(string $charset)
    {
        $this->decorated->setCharset($charset);
    }

    public function getCharset(): string
    {
        return $this->decorated->getCharset();
    }

    public function hasExtension(string $class): bool
    {
        return $this->decorated->hasExtension($class);
    }

    public function addRuntimeLoader(RuntimeLoaderInterface $loader)
    {
        $this->decorated->addRuntimeLoader($loader);
    }

    public function getExtension(string $class): ExtensionInterface
    {
        return $this->decorated->getExtension($class);
    }

    public function getRuntime(string $class)
    {
        return $this->decorated->getRuntime($class);
    }

    public function addExtension(ExtensionInterface $extension)
    {
        $this->decorated->addExtension($extension);
    }

    public function setExtensions(array $extensions)
    {
        $this->decorated->setExtensions($extensions);
    }

    public function getExtensions(): array
    {
        return $this->decorated->getExtensions();
    }

    public function addTokenParser(TokenParserInterface $parser)
    {
        $this->decorated->addTokenParser($parser);
    }

    public function getTokenParsers(): array
    {
        return $this->decorated->getTokenParsers();
    }

    public function getTokenParser(string $name): ?TokenParserInterface
    {
        return $this->decorated->getTokenParser($name);
    }

    public function registerUndefinedTokenParserCallback(callable $callable): void
    {
        $this->decorated->registerUndefinedTokenParserCallback($callable);
    }

    public function addNodeVisitor(NodeVisitorInterface $visitor)
    {
        $this->decorated->addNodeVisitor($visitor);
    }

    public function getNodeVisitors(): array
    {
        return $this->decorated->getNodeVisitors();
    }

    public function addFilter(TwigFilter $filter)
    {
        $this->decorated->addFilter($filter);
    }

    public function getFilter(string $name): ?TwigFilter
    {
        return $this->decorated->getFilter($name);
    }

    public function registerUndefinedFilterCallback(callable $callable): void
    {
        $this->decorated->registerUndefinedFilterCallback($callable);
    }

    public function getFilters(): array
    {
        return $this->decorated->getFilters();
    }

    public function addTest(TwigTest $test)
    {
        $this->decorated->addTest($test);
    }

    public function getTests(): array
    {
        return $this->decorated->getTests();
    }

    public function getTest(string $name): ?TwigTest
    {
        return $this->decorated->getTest($name);
    }

    public function addFunction(TwigFunction $function)
    {
        $this->decorated->addFunction($function);
    }

    public function getFunction(string $name): ?TwigFunction
    {
        return $this->decorated->getFunction($name);
    }

    public function registerUndefinedFunctionCallback(callable $callable): void
    {
        $this->decorated->registerUndefinedFunctionCallback($callable);
    }

    public function getFunctions(): array
    {
        return $this->decorated->getFunctions();
    }

    public function addGlobal(string $name, $value)
    {
        $this->decorated->addGlobal($name, $value);
    }

    public function getGlobals(): array
    {
        return $this->decorated->getGlobals();
    }

    public function mergeGlobals(array $context): array
    {
        return $this->decorated->mergeGlobals($context);
    }

    public function getUnaryOperators(): array
    {
        return $this->decorated->getUnaryOperators();
    }

    public function getBinaryOperators(): array
    {
        return $this->decorated->getBinaryOperators();
    }

}
