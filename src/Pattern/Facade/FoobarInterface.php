<?php

/*
 * This file is part of the contact package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Pattern\Facade;

/**
 * Interface FoobarInterface
 *
 * @author Benjamin Georgeault
 */
interface FoobarInterface
{
    public function getFoo(): string;
}
