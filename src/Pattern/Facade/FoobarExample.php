<?php

/*
 * This file is part of the contact package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Pattern\Facade;

/**
 * Class FoobarExample
 *
 * @author Benjamin Georgeault
 */
class FoobarExample implements FoobarInterface
{
    public function getFoo(): string
    {
        return 'foobar';
    }
}
