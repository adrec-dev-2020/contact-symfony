<?php

/*
 * This file is part of the contact package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Pattern\Facade;

/**
 * Class AnotherFoobar
 *
 * @author Benjamin Georgeault
 */
class AnotherFoobar implements FoobarInterface
{
    public function getFoo(): string
    {
        return 'barfoo';
    }
}
